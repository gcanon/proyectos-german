package daos;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Esta clase representa un DAO
 * @author German Dario Ca�on Nieto
 */
public abstract class Dao< Entidad, Llave extends Serializable >{

	//--------------------------------------------------------------------------------
	// Atributos 
	//--------------------------------------------------------------------------------
	
	@PersistenceContext(unitName="Colegio")
	/** Permite comunicar con la base de datos */
	protected EntityManager manager;
	
	//--------------------------------------------------------------------------------
	// Metodos 
	//--------------------------------------------------------------------------------
	
	/**
	 * Permite insertar o adicionar una entidad a la base de datos
	 * @param entidad La entidad a insertar en la base de datos
	 */
	public void insert(Entidad entidad){
		manager.persist(entidad);
	}
	
	/**
	 * Permite buscar una entidad de la base de datos dada su llave 
	 * @param llave La llave de la entidad a encontrar
	 * @return La entidad si esta existe sino retorna null
	 */
	public Entidad findByKey(Llave llave){
		return manager.find(getEntityClass(), llave);
	}
	
	/**
	 * Permite actualizar una entidad de la base de datos
	 * @param entidad La entidad a actualizar
	 */
	public void update(Entidad entidad){
		manager.merge(entidad);
	}
	
	/**
	 * Obtiene todos los objetos que existen de una entidad
	 * @return Retorna la lista de objetos de dicha entidad
	 */
	public List<Entidad> getAll() {
		return manager.createQuery("select entidad from " + getEntityClass().getSimpleName()
		+ " entidad ", getEntityClass()).getResultList();
	}
	
	/**
	 * Obtiene una lista de objetos de una entidad segun una condicion dada
	 * @param condicion La condicion que deben cumplir las entidades a extraer
	 * @return La lista de entidades que cumplen dicha condicion
	 */
	public List<Entidad> select(String condicion) {
		return manager.createQuery("select entidad from " + getEntityClass().getSimpleName()+ " entidad "
		+ ("".equals(condicion) ? "" : (condicion == null ? ""
		: " where " + condicion)), getEntityClass()).getResultList();
	}
	
	/**
	 * Permite remover o eliminar una entidad de la base de datos
	 * @param entidad La entidad a eliminar de la base de datos
	 */
	public void remove(Entidad entidad) {
		manager.remove(manager.merge(entidad));
	}
	
	/**
	 * Obtiene la entidad 
	 * @return La entidad
	 */
	protected abstract Class<Entidad> getEntityClass();
	
}
