package entidades;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Colegio
 * Esta clase representa un Colegio
 * @author German Dario Ca�on Nieto
 */
@SuppressWarnings("serial")
@Entity
public class Colegio implements Serializable {

	//----------------------------------------------------------------------------------
	// Atributos
	//----------------------------------------------------------------------------------
	
	@Id
	/** El codigo �nico que identifica al colegio */
	private int codigo;
	
	/** Nombre del Colegio */
	private String nombre;
	
	/** Los estudiantes del colegio */
	@OneToMany(mappedBy = "colegio")
	private List<Estudiante> estudiantes;
	
	//----------------------------------------------------------------------------------
	// M�todos
	//----------------------------------------------------------------------------------
	
	/**
	 * Retorna el codigo �nico del Colegio
	 * @return El codigo �nico del Colegio
	 */
	public int getCodigo() {
		return this.codigo;
	}

	/**
	 * Asigna el codigo al Colegio
	 * @param codigo El codigo a asignar
	 */
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}   
	
	/**
	 * Retorna el nombre del Colegio
	 * @return El nombre del Colegio
	 */
	public String getNombre() {
		return this.nombre;
	}

	/**
	 * Asigna el nombre al Colegio
	 * @param nombre El nombre a asignar
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Obtiene todos los estudiantes del colegio
	 * @return Todos los estudiantes del colegio
	 */
	public List<Estudiante> getEstudiantes() {
		return estudiantes;
	}

	/**
	 * Asigna todos los estudiantes al colegio
	 * @param estudiantes Los estudianbtes a asignar al colegio
	 */
	public void setEstudiantes(List<Estudiante> estudiantes) {
		this.estudiantes = estudiantes;
	}
   
}
