package entidades;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Estudiante
 * Esta clase representa un Estudiante del Colegio
 * @author German Dario Ca�on Nieto
 */
@SuppressWarnings("serial")
@Entity
public class Estudiante implements Serializable {

	//----------------------------------------------------------------------------------
	// Atributos
	//----------------------------------------------------------------------------------
	
	@Id
	/** La cedula �nica que identifica el estudiante*/
	private String cedula;
	
	/** El nombre del Estudiante */
	private String nombre;
	
	/** El apellido del Estudiante */
	private String apellido;
	
	/** El sexo del Estuidiante */
	private String sexo;
	
	/** El colegio en el que esta el estudiante */
	@ManyToOne
	private Colegio colegio;

	//----------------------------------------------------------------------------------
	// M�todos
	//----------------------------------------------------------------------------------

	/**
	 * Retorna la cedula del estudiante
	 * @return La cedula del estudiante
	 */
	public String getCedula() {
		return this.cedula;
	}

	/**
	 * Asigna la cedula al estudiante
	 * @param cedula La cedula a asignar
	 */
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}  
	
	/**
	 * Retorna el nombre del estudiante
	 * @return El nombre del estudiante
	 */
	public String getNombre() {
		return this.nombre;
	}

	/**
	 * Asigna el nombre al estudiante
	 * @param nombre El nombre a asignar
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}   
	
	/**
	 * Retorna el apellido del estudiante
	 * @return El apellido del estudiante
	 */
	public String getApellido() {
		return this.apellido;
	}

	/**
	 * Asigna el apellido al estudiante
	 * @param apellido El apellido a asignar
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}   
	
	/**
	 * Retorna el sexo del estudiante
	 * @return El sexo del estudiante
	 */
	public String getSexo() {
		return this.sexo;
	}

	/**
	 * Asigna el sexo al estudiante
	 * @param sexo El sexo a asignar
	 */
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	/**
	 * Obtiene el colegio del estudiante
	 * @return El colegio del estudiante
	 */
	public Colegio getColegio() {
		return colegio;
	}

	/**
	 * Asigna el colegio al estudiante
	 * @param colegio El colegio a asignar al estudiante
	 */
	public void setColegio(Colegio colegio) {
		this.colegio = colegio;
	}
   
}
