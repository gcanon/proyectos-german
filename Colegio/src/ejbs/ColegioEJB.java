package ejbs;

import javax.ejb.Stateless;

import daos.Dao;
import entidades.Colegio;

/**
 * Esta clase representa el EJB de Colegio
 * @author German Dario Ca�on Nieto
 */
@Stateless
public class ColegioEJB extends Dao<Colegio, Integer>{

	//----------------------------------------------------------------------------------
	// M�todos
	//----------------------------------------------------------------------------------
	
	/**
	 * Obtiene la entidad
	 * @return La entidad
	 */
	@Override
	protected Class<Colegio> getEntityClass() {
		return Colegio.class;
	}
	
}
