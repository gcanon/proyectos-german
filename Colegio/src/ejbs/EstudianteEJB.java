package ejbs;

import javax.ejb.Stateless;

import daos.Dao;
import entidades.Estudiante;

/**
 * Esta clase representa el EJB de Estudiante
 * @author German Dario Ca�on Nieto
 */
@Stateless
public class EstudianteEJB extends Dao<Estudiante, String>{

	//----------------------------------------------------------------------------------
	// M�todos
	//----------------------------------------------------------------------------------
	
	/**
	 * Obtiene la entidad
	 * @return La entidad
	 */
	@Override
	protected Class<Estudiante> getEntityClass() {
		return Estudiante.class;
	}

	
}
