package negocio;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import ejbs.ColegioEJB;
import ejbs.EstudianteEJB;
import entidades.Colegio;
import entidades.Estudiante;

/**
 * Session Bean implementation class Gestion
 * @author German Dario Ca�on Nieto
 */
@Stateless
@LocalBean
public class Gestion implements GestionRemote, GestionLocal {

	//----------------------------------------------------------------------------------
	// Atributos
	//----------------------------------------------------------------------------------
	
	@EJB
	/** El EJB de Colegio */
	private ColegioEJB colegioEJB;
	
	@EJB
	/** El EJB del Estudiante */
	private EstudianteEJB estudianteEJB;
	
	//----------------------------------------------------------------------------------
	// M�todos
	//----------------------------------------------------------------------------------
	
	/**
	 * Adiciona un estudiante a la base de datos
	 * @param estudiante El estudiante a adicionar
	 */
	public void adicionarEstudiante(Estudiante estudiante) {
		estudianteEJB.insert(estudiante);
	}
	
	/**
	 * Actualiza un estudiante de la base de datos
	 * @param estudiante El estudiante a actualizar
	 */
	public void actualizarEstudiante(Estudiante estudiante) {
		Estudiante estudianteActual = estudianteEJB.findByKey(estudiante.getCedula());
		estudianteActual.setNombre(estudiante.getNombre());
		estudianteActual.setApellido(estudiante.getApellido());
		estudianteActual.setSexo(estudiante.getSexo());
		estudianteActual.setColegio(estudiante.getColegio());
		estudianteEJB.update(estudianteActual);
	}
	
	/**
	 * Elimina un estudiante de la base de datos
	 * @param estudiante El estudiante a eliminar
	 */
	public void eliminarEstudiante(Estudiante estudiante) {
		estudianteEJB.remove(estudiante);
	}
	
	/**
	 * Obtiene todos los estudiantes de la base de datos
	 * @return Todos los estudiantes de la base de datos
	 */
	public List<Estudiante> getEstudiantes(){
		return estudianteEJB.getAll();
	}
	
	/**
	 * Adiciona un colegio a la base de datos
	 * @param colegio El colegio a adicionar
	 */
	public void adicionarColegio(Colegio colegio) {
		colegioEJB.insert(colegio);
	}
	
	/**
	 * Obtiene todos los colegios de la base de datos
	 * @return Todos los colegios de la base de datos
	 */
	public List<Colegio> getColegios(){
		return colegioEJB.getAll();
	}
	
}
