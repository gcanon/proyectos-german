package beans;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * Esta clase representa las imagenes a mostrar 
 * @author German Dario Ca�on Nieto
 * @author Juan Sebastian Gonzalez Blandon
 */
@ManagedBean
@ViewScoped
public class ImagenesBean {

	//------------------------------------------------------------------------------------------------
	// Atributos
	//------------------------------------------------------------------------------------------------
	
	/** Son la lista de imagenes a mostrar */
	private ArrayList<String> imagenes;

	//------------------------------------------------------------------------------------------------
	// Constructor
	//------------------------------------------------------------------------------------------------
	
	/**
	 * Es el contructor de ImagenesBean
	 */
	public ImagenesBean() {
		imagenes = new ArrayList<String>(); 
		imagenes.add("auto1.jpg");
		imagenes.add("auto2.jpg");
		imagenes.add("auto3.jpg");
		imagenes.add("auto4.jpg");
		imagenes.add("auto5.jpg");
		imagenes.add("auto6.jpg");
	}

	//------------------------------------------------------------------------------------------------
	// Metodos
	//------------------------------------------------------------------------------------------------

	/**
	 * Obtiene las imagenes a mostrar
	 * @return imagenes Las imagenes a mostrar
	 */
	public ArrayList<String> getImagenes() {
		return imagenes;
	}

	/**
	 * Asigna las imagenes a mostrar
	 * @param imagenes Las imagenes a mostrar
	 */
	public void setImagenes(ArrayList<String> imagenes) {
		this.imagenes = imagenes;
	}
	
}
