package beans;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import negocio.Gestion;
import entidades.Colegio;
import entidades.Estudiante;

@ManagedBean
@ViewScoped
public class AdministracionBean {
	
	//----------------------------------------------------------------------------------
	// Atributos
	//----------------------------------------------------------------------------------
	
	@EJB(name = "Gestion/local")
	private Gestion gestion;
	
	/** El estudiante a agregar en la base de datos*/
	private Estudiante estudiante = new Estudiante();
	
	/** El colegio en el que esta el estudiante a agregar */
	private Colegio colegio = new Colegio();
	
	/** Es la accion a realizar */
	private String accion = "registrar";

	//----------------------------------------------------------------------------------
	// M�todos
	//----------------------------------------------------------------------------------
	
	/**
	 * Adiciona un estudiante a la base de datos
	 */
	public void adicionarEstudiante(){
		try {
			gestion.adicionarEstudiante(estudiante);
			estudiante = new Estudiante();
			FacesMessage mensaje = new FacesMessage("El estudiante ha sido agregado");
			FacesContext.getCurrentInstance().addMessage(null, mensaje);
		} catch (Exception e) {
			FacesMessage mensaje = new FacesMessage(FacesMessage.SEVERITY_ERROR ,"El estudiante ya existe","El estudiante ya existe");
			FacesContext.getCurrentInstance().addMessage(null, mensaje);
		}
	}

	/**
	 * Actualiza un estudiante de la base de datos
	 */
	public void actualizarEstudiante(){
		gestion.actualizarEstudiante(estudiante);
		FacesMessage mensaje = new FacesMessage("El estudiante ha sido actualizado");
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
		estudiante = new Estudiante();
		accion = "registrar";
	}

	/**
	 * Elimina un estudiante de la base de datos
	 * @return la pagina que se redireccionara
	 */
	public String eliminarAdministrativoDeBD(){
		gestion.eliminarEstudiante(estudiante);
		FacesMessage mensaje = new FacesMessage("El estudiante ha sido eliminado");
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
		return "/paginas/crud/crudEstudiante.xhtml";
	}
	
	/**
	 * Obtiene todos los estudiantes de la base de datos
	 * @return Todos los estudiantes de la base de datos
	 */
	public List<Estudiante> getEstudiantes(){
		return gestion.getEstudiantes();
	}
	
	/**
	 * Adiciona un colegio a la base de datos
	 * @param colegio El colegio a adicionar
	 */
	public void adicionarColegio(){
		try {
			gestion.adicionarColegio(colegio);
			colegio = new Colegio();
			FacesMessage mensaje = new FacesMessage("El colegio ha sido agregado");
			FacesContext.getCurrentInstance().addMessage(null, mensaje);
		} catch (Exception e) {
			FacesMessage mensaje = new FacesMessage(FacesMessage.SEVERITY_ERROR ,"El colegio ya existe","El colegio ya existe");
			FacesContext.getCurrentInstance().addMessage(null, mensaje);
		}
	}
	
	/**
	 * Obtiene todos los colegios de la base de datos
	 * @return Todos los colegios de la base de datos
	 */
	public List<Colegio> getColegios(){
		return gestion.getColegios();
	}
	
	/**
	 * Modifica la accion a realizar
	 */
	public void cancelar(){
		accion = "registrar";
		estudiante = new Estudiante();
		colegio = new Colegio();
	}
	
	/**
	 * Obtiene la accion a realizar
	 * @return La accion a realizar
	 */
	public String getAccion() {
		return accion;
	}

	/**
	 * Asigna la accion a realizar
	 * @param accion La accion a realizar
	 */
	public void setAccion(String accion) {
		this.accion = accion;
	}
	
	
}
